FROM resin/rpi-raspbian:wheezy
MAINTAINER Mitsukuni Sato <mitsukuni.sato@gmail.com>

RUN apt-get update
RUN apt-get install -y python2.7 python2.7-dev
